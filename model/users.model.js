const md5 = require('md5');
const { Op } = require('sequelize');
const db = require('../db/models');

class UserModel {
  // for register
  /* recordNewData = (dataBody) => {
    db.User.create({
      username: dataBody.username,
      email: dataBody.email,
      password: md5(dataBody.password),
    });

  } */
  // temp
  static async recordNewData(dataBody) {
    const newUser = await db.User.create({
      username: dataBody.username,
      email: dataBody.email,
      password: md5(dataBody.password),
    });
    // const newUser = dataBody;
    /* const user_id = newUser.id;
    const newUserBio = await db.User_Biodata.create({
      user_id: user_id,
    }); */
    return newUser;
  }

  static async isUserRegistered(dataBody) {
    const existUser = await db.User.findOne({
      where: {
        [Op.or]: [{ username: dataBody.username }, { email: dataBody.email }],
      },
    });

    if (existUser) {
      return true;
    }
    return false;
  }

  // for update bio
  static async getSingleUser(idUser) {
    const data = await db.User.findOne({
      include: [db.User_Biodata],
      where: { id: idUser },
    });
    return data;
  }

  // getSingleUser= async (id) => {
  //   const dataUser = await db.User.findOne({ where: { id: id } });
  //   console.log(dataUser);
  //   return dataUser;
  // }
  static async getSingleBio(id) {
    const dataUser = await db.User_Biodata.findOne({ where: { user_id: id } });
    // console.log(dataUser);
    return dataUser;
  }

  static async updateSingleBio(id, dataBody) {
    const dataUser = await db.User_Biodata.update(
      {
        fullname: dataBody.fullname,
        address: dataBody.address,
        phone_number: dataBody.phone_number,
        photo_url: dataBody.photo_url,
      },
      { where: { user_id: id } },
    );
    // console.log("data model" + dataBody);
    return dataUser;
  }

  static async createSingleBio(id, dataBody) {
    const dataUser = await db.User_Biodata.create({
      user_id: id,
      fullname: dataBody.fullname,
      address: dataBody.address,
      phone_number: dataBody.phone_number,
      photo_url: dataBody.photo_url,
    });
    // console.log("data model" + dataBody);
    return dataUser;
  }

  static async loginUser(userName, password) {
    // loginUser = async (req,res) => {

    // const { username, password } = req.body;
    const dataUser = await db.User.findOne({
      where: {
        username: userName,
        password: md5(password),
      },
      attributes: { exclude: 'password' },
      raw: true,
    });
    // console.log(dataUser);
    return dataUser;
  }
}

module.exports = UserModel;
