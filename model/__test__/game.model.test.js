/* eslint-disable no-undef */
const {
  getAllDataRooms, isRoomNameUsed, createNewRoom,
  getGameHistory, recordGameVsCom, joinRoom, getSingleRoom, RoomResult,
} = require('../game.model');

describe('game.model', () => {
  test('isRoomNameUsed should identify room existed', async () => {
    const response = await isRoomNameUsed('rahman 1');

    expect(response).toBe(true);
  });

  test('isRoomNameUsed should identify room existed', async () => {
    const response = await isRoomNameUsed('rahman');

    expect(response).toBe(false);
  });

  test('createNewRoom should add new room that produce room Id ', async () => {
    const response = await createNewRoom(65, 'zuhri test 2', 'rock');

    expect(response.dataValues.pilihan_p1).toBe('rock');
    expect(response.dataValues.room_name).toBe('zuhri test 2');
  });

  test('getAllDataRooms should find all room', async () => {
    const response = await getAllDataRooms();

    expect(response[0].dataValues.room_name).toBe('ROOM BARU');
  });

  // test('getAllDataRooms should render error message when API fail', async () => {
  //   const response = await getAllDataRooms('room');
  //   console.log(response[0]);

  //   expect(true).toBe(true);
  // });

  test('getGameHistory should find single user game history', async () => {
    const response = await getGameHistory(64);
    const condition = response[0].dataValues.player1_id || response[0].dataValues.player2_id === 64;

    expect(condition).toBe(64);
  });

  // test('getGameHistory should render error message when API fail', async () => {
  //   const response = await getGameHistory(64);
  //   console.log(response[0]);
  // eslint-disable-next-line max-len
  //   const condition = response[0].dataValues.player1_id || response[0].dataValues.player2_id === 64;

  //   expect(condition).toBe(64);
  // });

  test('recordGameVsCom should create the game player vs com', async () => {
    const response = await recordGameVsCom(64, 'rock', 'paper', 'win', 'zuhri', 'win', 'lose');

    expect(response.dataValues.lawan_id).toBe(null);
  });

  // test('recordGameVsCom should render error message when API fail', async () => {
  //   const response = await recordGameVsCom(64, 'rock', 'paper', 'win', 'zuhri', 'win', 'lose');
  //   console.log(response);

  //   expect(response.dataValues.lawan_id).toBe(null);
  // });

  test('joinRoom should create match player vs player accessed by player 2', async () => {
    const response = await joinRoom('rock', 64, 625);

    expect(response).toStrictEqual([1]);
  });

  // test('joinRoom should render error message when API fail', async () => {
  //   const response = await joinRoom('rock', 64, 616);
  //   console.log(response);

  //   expect(true).toBe(true);
  // });

  test('getSingleRoom should find single room by id user', async () => {
    const response = await getSingleRoom(622);

    expect(response.dataValues.room_name).toBe('khadijah day 4');
    expect(response.dataValues.pilihan_p1).toBe('scissors');
    expect(response.dataValues.pilihan_p2).toBe('rock');
  });

  // test('getSingleRoom should render error message when API fail', async () => {
  //   const response = await getSingleRoom(622);
  //   console.log(response);

  //   expect(response.dataValues.room_name).toBe('khadijah day 4');
  //   expect(response.dataValues.pilihan_p1).toBe('scissors');
  //   expect(response.dataValues.pilihan_p2).toBe('rock');
  // });

  test('RoomResult should make the result of the match player vs player', async () => {
    const response = await RoomResult(
      'rock',
      'lose',
      'win',
      666,
      65,
      64,
      'zuhri',
    );

    expect(response).toStrictEqual([1]);
  });
});
