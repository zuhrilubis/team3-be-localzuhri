/* eslint-disable no-undef */
const { Op } = require('sequelize');
const db = require('../db/models');

class GameModel {
  // Check is room name already exist or not yet
  static async isRoomNameUsed(roomName) {
    const namedRoom = await db.Room.findOne({
      where: { room_name: roomName },
    });

    if (namedRoom) {
      return true;
    }
    return false;
  }

  // Query to Create room (record in 'Rooms' and 'Games' tables)
  static async createNewRoom(player1Id, roomName, pilihanP1) {
    const newAddRoom = await db.Room.create({
      player1_id: player1Id,
      room_name: roomName,
      pilihan_p1: pilihanP1,
      status: 'available',
    });
    return newAddRoom;
  }

  // Query to get all rooms
  static async getAllDataRooms() {
    try {
      return await db.Room.findAll({
        // raw: true,
        where: {
          [Op.not]: { room_name: 'VS COM' },
        },
        attributes: {
          exclude: [
            'player1_id',
            'player2_id',
            'pilihan_p1',
            'pilihan_p2',
            'hasil_1',
            'hasil_2',
            'createdAt',
            'updatedAt',
          ],
        },
        include: [
          {
            model: db.User,
            as: 'player1',
            attributes: {
              exclude: ['id', 'email', 'password', 'createdAt', 'updatedAt'],
            },
          },
          {
            model: db.User,
            as: 'player2',
            attributes: {
              exclude: ['id', 'email', 'password', 'createdAt', 'updatedAt'],
            },
          },
        ],
      });
    } catch (error) {
      return res.status(400).json({ error });
    }
  }

  // Query to get game history
  static async getGameHistory(idUser) {
    try {
      return await db.Room.findAll({
        attributes: [
          'room_name',
          'player1_id',
          'player2_id',
          // "pilihan_p1",
          // "pilihan_p2",
          'pemenang',
          // "status",
          'createdAt',
          'updatedAt',
          'hasil_1',
          'hasil_2',
        ],
        where: {
          [Op.or]: [{ player1_id: idUser }, { player2_id: idUser }],
          [Op.not]: { status: 'available' },
        },
        // raw: true,
        // include: [
        //   {
        //     model: db.Game,
        //     attributes: ["hasil"],
        //     where: {
        //       [Op.or]: [{ player_id: idUser }, { lawan_id: idUser }],
        //     },
        //   },
        // ],
      });
    } catch (error) {
      // eslint-disable-next-line no-undef
      return res.status(400).json({ error });
    }
  }

  // -----player vs com------
  static async recordGameVsCom(
    player1Id,
    pilihanP1,
    pilihanP2,
    hasil,
    pemenang,
    hasil1,
    hasil2,
  ) {
    try {
      const recordedGameVsComRoom = await db.Room.create({
        room_name: 'VS COM',
        player1_id: player1Id,
        pilihan_p1: pilihanP1,
        pilihan_p2: pilihanP2,
        pemenang,
        status: 'complete',
        hasil_1: hasil1,
        hasil_2: hasil2,
      });

      const roomId = recordedGameVsComRoom.id;
      const recordedGameVsCom = await db.Game.create({
        player_id: player1Id,
        room_id: roomId,
        pilihan: pilihanP1,
        hasil,
      });
      return (recordedGameVsComRoom, recordedGameVsCom);
    } catch (error) {
      return res.status(400).json({ error });
    }
  }

  // Player 1 vs Player 2
  static async joinRoom(player2, token, idRoom) {
    try {
      // eslint-disable-next-line radix
      const roomId = parseInt(idRoom);
      const updateRoom = await db.Room.update(
        {
          pilihan_p2: player2.pilihan_p2,
          player2_id: token,
        },
        { where: { id: roomId } },
      );
      return updateRoom;
    } catch (error) {
      return res.status(400).json({ error: error.message });
    }
  }

  static async getSingleRoom(id) {
    try {
      // eslint-disable-next-line radix
      const roomId = parseInt(id);
      const singleRoom = await db.Room.findOne({
        where: { id: roomId },

        // include: [
        //   {
        //     model: db.User,
        //     as: "Player 1",
        //   },
        //   {
        //     model: db.User,
        //     as: "Player 2",
        //   },
        // ],
      });
      return singleRoom;
    } catch (error) {
      // eslint-disable-next-line no-undef
      return res.status(400).json({ error: error.message });
    }
  }

  static async RoomResult(
    player2,
    hasilPlayer1,
    hasilPlayer2,
    idRoom,
    idUser,
    token,
    winner,
  ) {
    try {
      // eslint-disable-next-line radix
      const roomId = parseInt(idRoom);
      const updateRoomResult = await db.Room.update(
        {
          hasil_1: hasilPlayer1,
          hasil_2: hasilPlayer2,
          pemenang: winner,
          status: 'Complete',
        },
        { where: { id: roomId } },
      );

      const updateGameResult = await db.Game.update(
        {
          player_id: token,
          pilihan: player2.pilihan_p2,
          lawan_id: idUser,
          hasil: `${winner} win`,
        },
        { where: { room_id: roomId } },
      );
      return (updateRoomResult, updateGameResult);
    } catch (error) {
      // eslint-disable-next-line no-undef
      return res.status(400).json({ error: error.message });
    }
  }
}

module.exports = GameModel;
