const gameModel = require('../model/game.model');
const usersModel = require('../model/users.model');
const { theGameResult } = require('../schema/whatIsTheResult');

class GameController {
  static async createRoom(req, res) {
    const player1Id = req.token.id;
    const { roomName, pilihanP1 } = req.body;

    // Check is room name already created or not
    const namedRoom = await gameModel.isRoomNameUsed(roomName);
    if (namedRoom) {
      return res.status(409).json({
        message: 'The Room Name already in use! Please use another name',
      });
    }

    try {
      // If room name hasn't been created yet, then create new room
      const createdRoom = await gameModel.createNewRoom(
        player1Id,
        roomName,
        pilihanP1,
      );
      // console.log(createdRoom);
      const createdRoomId = createdRoom.dataValues.id;
      const singleRoom = await gameModel.getSingleRoom(createdRoomId);
      const pilihan1 = singleRoom.pilihan_p1;
      if (createdRoom) {
        return res
          .status(201)
          .json({
            message: 'Success creating new room!',
            room_id: createdRoomId,
            pilihan_1: pilihan1,
          });
      }
    } catch (error) {
      return res.status(400).json({ error });
    }
    return null;
  }

  static async getAllRooms(req, res) {
    try {
      const rooms = await gameModel.getAllDataRooms();
      return res.json(rooms);
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  }

  static async getGameHistories(req, res) {
    const idUser = req.token.id;
    try {
      // Searching game history
      const gameHistories = await gameModel.getGameHistory(idUser);
      if (gameHistories === null) {
        res.statusCode = 400;
        return res.json({ message: 'Cannot find game history!' });
      }

      if (gameHistories) {
        return res.json(gameHistories);
      }
    } catch (error) {
      res.statusCode = 404;
      return res.json({ error: error.message });
    }
    return null;
  }

  // -----player vs com------
  static async recordGameVsCom(req, res) {
    try {
      const player1Id = req.token.id;
      const { username } = req.token;
      const { pilihanP1, pilihanP2 } = req.body;

      const [hasilPlayer, hasilCom] = theGameResult(pilihanP1, pilihanP2);

      let pemenang = '';
      if (hasilPlayer === 'win' && hasilCom === 'lose') {
        pemenang = username;
      } else if (hasilPlayer === 'lose' && hasilCom === 'win') {
        pemenang = 'COM';
      } else if (hasilPlayer === 'draw' && hasilCom === 'draw') {
        pemenang = 'DRAW';
      }

      const hasil1 = hasilPlayer;
      const hasil2 = hasilCom;

      await gameModel.recordGameVsCom(
        player1Id,
        pilihanP1,
        pilihanP2,
        hasilPlayer,
        pemenang,
        hasil1,
        hasil2,
      );

      return res.json({
        message: 'The Room VS COM are successfully created',
        username_: username,
        Player_id: player1Id,
        pilihan_player: pilihanP1,
        pilihan_com: pilihanP2,
        pemenang_: pemenang,
        hasil_1: hasilPlayer,
        hasil_2: hasilCom,
      });
    } catch (error) {
      return res.status(400).json({ error });
    }
  }

  // getCompleteRoom
  static async completeRoom(req, res) {
    const { idRoom } = req.params;

    const singleRoom = await gameModel.getSingleRoom(idRoom);

    const user1 = await usersModel.getSingleUser(singleRoom.player1_id);
    const user2 = await usersModel.getSingleUser(singleRoom.player2_id);

    const username1 = user1.username;
    let username2;

    if (singleRoom.status === 'Complete') {
      if (user2 !== null) {
        username2 = user2.username;
      } else {
        return 'User2 is null.';
      }
      const roomName = singleRoom.room_name;
      const player1Id = singleRoom.player1_id;
      const player2Id = singleRoom.player2_id;
      const pilihanP1 = singleRoom.pilihan_p1;
      const pilihanP2 = singleRoom.pilihan_p2;
      const hasil1 = singleRoom.hasil_1;
      const hasil2 = singleRoom.hasil_2;
      const { pemenang } = singleRoom;

      return res.json({
        roomName,
        player1Id,
        player2Id,
        username1,
        username2,
        pilihanP1,
        pilihanP2,
        hasil1,
        hasil2,
        pemenang,
      });
    }
    return null;
  }
  // Player1 vs Player2

  static async getMyRoom(req, res) {
    const { idRoom } = req.params;

    const singleRoom = await gameModel.getSingleRoom(idRoom);
    const user1 = await usersModel.getSingleUser(singleRoom.player1_id);
    const roomName = singleRoom.room_name;
    const pilihanP1 = singleRoom.pilihan_p1;
    const username1 = user1.username;

    if (singleRoom.status === 'available') {
      return res.json({ roomName, username1, pilihanP1 });
    }
    return res.json(roomName, username1, pilihanP1);
  }

  static async joinRoom(req, res) {
    const player2 = req.body;
    const token = req.token.id;
    const usernameP2 = req.token.username;
    const { idRoom } = req.params;

    // console.log(idRoom);

    // return res.json({message: " tes aja dlu"})

    try {
      if (
        player2.pilihan_p2 !== 'rock'
        && player2.pilihan_p2 !== 'paper'
        && player2.pilihan_p2 !== 'scissors'
      ) {
        return res
          .status(400)
          .json({ message: 'Format pilihan harus rock/paper/scissors' });
      }
      const singleRoom = await gameModel.getSingleRoom(idRoom);

      // console.log(singleRoom.player1_id);
      // console.log(singleRoom.status);

      // player dilarang melawan dirinya sendiri
      if (singleRoom.player1_id === token) {
        res.statusCode = 400;
        return res.json({ message: 'Cannot Play with Your Self!' });
      }

      // //player tidak dapat bermain di room yang sudah complete
      if (singleRoom.status === 'Complete') {
        res.statusCode = 400;
        return res.json({ message: 'Room Complete!' });
      }

      // // input pilihan player2 ke dalam data base
      await gameModel.joinRoom(player2, token, idRoom);

      const p1Status = singleRoom.pilihan_p1;
      const p2Status = player2.pilihan_p2;

      const idUser = singleRoom.player1_id;
      const usernameP1 = await usersModel.getSingleUser(idUser);
      const playerOneUsername = usernameP1.username;

      // // // Menentukan pemenang
      const [hasilPlayer1, hasilPlayer2] = theGameResult(p1Status, p2Status);
      // judgementRPS.judgementRPS(p1Status, p2Status);
      // console.log(result);

      let pemenang = '';

      if (hasilPlayer1 === 'win' && hasilPlayer2 === 'lose') {
        pemenang = playerOneUsername;
      } else if (hasilPlayer1 === 'lose' && hasilPlayer2 === 'win') {
        pemenang = usernameP2;
      } else if (hasilPlayer1 === 'draw' && hasilPlayer2 === 'draw') {
        pemenang = 'DRAW';
      }
      // // Menambahkan hasil pada database
      const RoomResult = await gameModel.RoomResult(
        player2,
        hasilPlayer1,
        hasilPlayer2,
        idRoom,
        idUser,
        token,
        pemenang,
      );

      res.json({
        message: 'Sukses Melawan',

        player_id_1: idUser,
        player_id_2: token,
        player_username_1: usernameP1.username,
        player_username_2: usernameP2,
        Pilihan_Player_1: p1Status,
        Pilihan_Player_2: p2Status,
        'Player 1 Result': hasilPlayer1,
        'Player 2 Result': hasilPlayer2,
        winner: pemenang,
        'Room Id': idRoom,
      });
      return RoomResult;
    } catch (error) {
      return res.status(400).json(error);
    }
  }
}

module.exports = GameController;
