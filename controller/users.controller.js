const jwtTool = require('jsonwebtoken');
const userModel = require('../model/users.model');
require('dotenv').config();

class UserController {
  static async registerUsers(req, res) {
    const dataBody = req.body;

    const existData = await userModel.isUserRegistered(dataBody);
    if (existData) {
      return res.json({ message: 'username or email is exist' });
    }
    userModel.recordNewData(dataBody);

    // return res.json(recordData);
    return res.json({ message: 'registration success' });
  }

  static async getSingleUser(req, res) {
    const idUser = req.token.id;

    try {
      const users = await userModel.getSingleUser(idUser);
      if (users) {
        return res.json(users);
      }
      res.statusCode = 400;
      return res.json({ message: `Data ${idUser} tidak ditemukan` });
      // return res.json(users);
    } catch (error) {
      res.statusCode = 400;
      return res.json(error);
    }
  }

  static async updateSingleBio(req, res) {
    const { id } = req.token;
    const dataBody = req.body;

    // console.log(dataBody);

    try {
      const userId = await userModel.getSingleUser(id);
      const userBio = await userModel.getSingleBio(id);
      // console.log("this id databody "+userBio)
      // return res.json({"message": userBio})

      if (userId) {
        if (userBio) {
          await userModel.updateSingleBio(id, dataBody);
          return res.json({ message: 'update successful' });
        }
        await userModel.createSingleBio(id, dataBody);
        return res.json({ message: 'update successful' });
      }
      return res.json({ message: 'user_id does not exist' });
    } catch (error) {
      return res.json({ message: 'error' });
    }
  }

  static async loginUser(req, res) {
    const { username, password } = req.body;
    try {
      const dataLogin = await userModel.loginUser(username, password);
      // console.log(dataLogin);
      // return res.json(dataLogin);
      if (dataLogin) {
        const token = jwtTool.sign(dataLogin, process.env.SECRET_KEY, {
          expiresIn: '1d',
        });
        /*
        const token = jwt.sign(
          { ...dataLogin, role: "player" },
          process.env.SECRET_KEY,
          { expiresIn: "1d" }
        ); */

        return res.json({ accessToken: token });
      }
      return res
        .status(400)
        .json({ message: 'User not found, wrong username/password' });
    } catch (error) {
      // console.log(error);
      return res.status(400).json({ message: 'User not found' });
    }
  }
}

module.exports = UserController;
