/* eslint-disable object-shorthand */
/* eslint-disable no-console */
/* eslint-disable quotes */
/* eslint-disable no-unused-vars */
/* eslint-disable import/order */
/* eslint-disable import/no-extraneous-dependencies */
const pdf = require('pdf-creator-node');
const fs = require('fs');
const gameModel = require('../model/game.model');

const getGameHistory = async (userId) => {
  const gameHistories = await gameModel.getGameHistory(userId);
  if (gameHistories === null) {
    throw new Error('Cannot find game history!');
  }
  console.log(gameHistories);
  console.log(typeof gameHistories);
  return gameHistories;
};

// eslint-disable-next-line import/prefer-default-export, arrow-body-style, consistent-return
const generatePdf = async (req, res) => {
  const html = fs.readFileSync("template.html", "utf-8");
  const options = {
    format: "Legal",
    orientation: "portrait",
    border: "10mm",
    header: {
      height: "10mm",
      contents: '<div style="text-align: center;">Game History</div>',
    },
  };
  const { id } = req.params;
  const gameHistories = await getGameHistory(id);
  const gameHistoriesArray = gameHistories.map((room) => room.toJSON());
  const document = {
    // eslint-disable-next-line object-shorthand
    html: html,
    data: { games: gameHistoriesArray },
    path: "output.pdf",
    type: "",
  };
  // eslint-disable-next-line no-console
  try {
    const pdfGenerated = await pdf.create(document, options);
    res.download(document.path, 'output.pdf');
    return document.path; // Return the path of the generated PDF
  } catch (error) {
    return { error };
  }
};
module.exports = {
  generatePdf,
};
