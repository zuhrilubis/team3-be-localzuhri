const express = require('express');

const gameRouter = express.Router();
const gameController = require('../controller/game.controller');
const authentication = require('../middleware/authentication');
// eslint-disable-next-line no-unused-vars
const protection = require('../middleware/protection');
const gameValidation = require('../validation/game.validator');
const schemaValidation = require('../middleware/schemaValidation');
const { generatePdf } = require('../controller/pdf.controller');
// const gameModel = require("../model/game.model");

// API to Create Room
gameRouter.post(
  '/create-room',
  authentication,
  gameValidation.createRoomValidRules(),
  schemaValidation,
  gameController.createRoom,
);

// API to Get all rooms
gameRouter.get('/rooms', authentication, gameController.getAllRooms);

// API to Get user's game history by ID
gameRouter.get(
  '/history',
  authentication,
  // protection,
  gameController.getGameHistories,
);

// API to Create Room Player VS Com
gameRouter.post(
  '/player-vs-com',
  authentication,
  gameValidation.recordGameVsComValidRules(),
  schemaValidation,
  gameController.recordGameVsCom,
);

gameRouter.put(
  '/player1-vs-player2/:idRoom',
  authentication,
  schemaValidation,
  gameController.joinRoom,
);

gameRouter.get(
  '/complete/:idRoom',
  authentication,
  gameController.completeRoom,
);
gameRouter.get('/room/:idRoom', authentication, gameController.getMyRoom);

gameRouter.get('/pdf/:id', async (req, res) => {
  try {
    await generatePdf(req, res);
    // Send the generated PDF as a response attachment
    // Provide a filename for the downloaded PDF
  } catch (error) {
    // console.error('Error generating or sending PDF:', error);
    res.json(error);
  }
});

module.exports = gameRouter;
