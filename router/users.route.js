const express = require('express');

const userRouter = express.Router();
const { checkSchema } = require('express-validator');
const userController = require('../controller/users.controller');
const authentication = require('../middleware/authentication');

// eslint-disable-next-line no-unused-vars
const protection = require('../middleware/protection');
// const userValidation = require('../validation/users.validator');
const schemaValidation = require('../middleware/schemaValidation');
const {
  registrationSchema,
  updateBioSchema,
  loginSchema,
} = require('../schema/users.schema');

// const userModel = require('../model/users.model');

// API to Register/Sign up
userRouter.post(
  '/daftar',
  checkSchema(registrationSchema),
  schemaValidation,
  userController.registerUsers,
);

// API cari detail user
userRouter.get('/detailUser', authentication, userController.getSingleUser);

// update 1 bio
userRouter.put(
  '/updateBio',
  checkSchema(updateBioSchema),
  schemaValidation,
  authentication,
  userController.updateSingleBio,
);

userRouter.post(
  '/login',
  checkSchema(loginSchema),
  schemaValidation,
  userController.loginUser,
);

module.exports = userRouter;
