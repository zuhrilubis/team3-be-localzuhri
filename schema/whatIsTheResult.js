const theGameResult = (choice1, choice2) => {
  if (
    (choice1 === 'rock' && choice2 === 'rock')
    || (choice1 === 'scissors' && choice2 === 'scissors')
    || (choice1 === 'paper' && choice2 === 'paper')
  ) {
    return ['draw', 'draw'];
  } if (
    (choice1 === 'rock' && choice2 === 'scissors')
    || (choice1 === 'scissors' && choice2 === 'paper')
    || (choice1 === 'paper' && choice2 === 'rock')
  ) {
    return ['win', 'lose'];
  } if (
    (choice1 === 'rock' && choice2 === 'paper')
    || (choice1 === 'scissors' && choice2 === 'rock')
    || (choice1 === 'paper' && choice2 === 'scissors')
  ) {
    return ['lose', 'win'];
  }
  return ['kenapa', 'kokbisa'];
};

function ComputerChoice() {
  const choices = ['rock', 'scissors', 'paper'];
  const randomIndex = Math.floor(Math.random() * choices.length);
  return choices[randomIndex];
}

module.exports = { theGameResult, ComputerChoice };
