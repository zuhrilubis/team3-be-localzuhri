const registrationSchema = {
  username: {
    isString: true,
    errorMessage: 'username should be string',
    // Add this line to assign the error message directly to "message" key
  },
  email: {
    isEmail: true,
    errorMessage: 'please provide correct email format',
    // Add this line to assign the error message directly to "message" key
  },
  password: {
    isLength: { options: { min: 8 } },
    errorMessage: 'Password should be at least 8 characters',
    // Add this line to assign the error message directly to "message" key
  },
};

const loginSchema = {
  username: {
    isString: true,
    errorMessage: 'username should be string',
  },
  password: {
    isLength: { options: { min: 8 } },
    errorMessage: 'Password should be at least 8 characters',
  },
};

const updateBioSchema = {
  fullname: {
    isString: true,
    errorMessage: 'full name should be string',
  },
  phone_number: {
    isMobilePhone: {
      options: ['any', { strictMode: false }],
    },
    errorMessage: 'incorrect format phone number',
  },
};
module.exports = { registrationSchema, updateBioSchema, loginSchema };
