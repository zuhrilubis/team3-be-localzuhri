const JWT = require('jsonwebtoken');
require('dotenv').config();

async function authorization(req, res, next) {
  // Get authorization header in "Authorization"
  const authHeader = req.headers.authorization;

  // Check if authorization header is present or not put in Bearer token
  if (!authHeader || !authHeader.startsWith('Bearer ')) {
    res.statusCode = 401;
    return res.json({ message: 'Unauthorized! Cannot use this method!' });
  }

  // Extract the token from the authorization header
  const token = authHeader.split(' ')[1];

  try {
    // Verify if the token is valid
    const decodedToken = await JWT.verify(token, process.env.SECRET_KEY);
    req.token = decodedToken;
    next();
  } catch (error) {
    res.statusCode = 401;
    return res.json({ message: 'Invalid token!' });
  }
  return null;
}

module.exports = authorization;
