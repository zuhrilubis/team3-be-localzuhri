const { validationResult } = require('express-validator');

const schemaValidation = (req, res, next) => {
  const result = validationResult(req);

  if (result.isEmpty()) {
    next();
  } else {
    const firstErrorMessage = result.array()[0].msg;
    res.status(400).json({ message: firstErrorMessage });
  }
};

module.exports = schemaValidation;
