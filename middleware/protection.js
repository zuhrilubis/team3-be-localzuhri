async function authProtection(req, res, next) {
  const tokenId = req.token.id.toString();
  const { idUser } = req.params;

  if (tokenId !== idUser) {
    return res.status(401).json({ message: 'Invalid token!' });
  } if (tokenId === idUser) {
    next();
  }
  return null;
}

module.exports = authProtection;
