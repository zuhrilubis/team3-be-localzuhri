const express = require('express');

const app = express();

const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const userRouter = require('./router/users.route');
const gameRouter = require('./router/game.route');
const gameSwagger = require('./gameSwagger.json');

app.use(cors());
app.use(express.json());

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(gameSwagger));

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.use('/users', userRouter);
app.use('/game', gameRouter);

module.exports = app;
