const { body } = require('express-validator');

class GameValidation {
  static createRoomValidRules() {
    return [
      body('roomName')
        .notEmpty()
        .withMessage('Room name cannot be empty, please input the room name')
        .isString()
        .withMessage('Room name should be letters, alphabets, or characters')
        .isLength({ min: 2 })
        .withMessage('Room name must be at least 2 characters')
        .isLength({ max: 100 })
        .withMessage('Room name must be maximum 100 characters'),

      body('pilihanP1')
        .notEmpty()
        .withMessage('Please choose your choice')
        .isString()
        .isIn(['rock', 'paper', 'scissors'])
        .withMessage('Choose between rock, paper, or scissors!'),
    ];
  }

  static recordGameVsComValidRules() {
    return [
      body('pilihanP1')
        .notEmpty()
        .withMessage('Please choose your choice')
        .isString()
        .isIn(['rock', 'paper', 'scissors'])
        .withMessage('Choose between rock, paper or scissors!'),
      body('pilihanP2')
        .notEmpty()
        .withMessage('Please choose your choice')
        .isString()
        .isIn(['rock', 'paper', 'scissors'])
        .withMessage('Choose between rock, paper or scissors!'),
    ];
  }
}

module.exports = GameValidation;
