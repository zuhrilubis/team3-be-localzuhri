/* eslint-disable no-unused-vars */
/* eslint-disable strict */

'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('User_Biodata', 'photo_url', {
      type: Sequelize.STRING,
      allowNull: true, // or false, depending on your requirements
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('User_Biodata', 'photo_url');
  },
};
