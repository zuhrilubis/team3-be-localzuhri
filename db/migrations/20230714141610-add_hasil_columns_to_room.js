/* eslint-disable no-unused-vars */
/* eslint-disable strict */

'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('Rooms', 'hasil_1', {
      type: Sequelize.STRING,
      allowNull: true,
    });

    await queryInterface.addColumn('Rooms', 'hasil_2', {
      type: Sequelize.STRING,
      allowNull: true,
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('Rooms', 'hasil_1');
    await queryInterface.removeColumn('Rooms', 'hasil_2');
  },
};
