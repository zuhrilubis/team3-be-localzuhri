/* eslint-disable strict */

'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  // eslint-disable-next-line camelcase
  class User_Biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // eslint-disable-next-line camelcase
      User_Biodata.belongsTo(models.User, { foreignKey: 'user_id' });
    }
  }
  // eslint-disable-next-line camelcase
  User_Biodata.init(
    {
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: 'user_id_unique',
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      fullname: {
        type: DataTypes.STRING,
      },
      phone_number: {
        type: DataTypes.STRING(100),
      },
      address: {
        type: DataTypes.STRING,
      },
      photo_url: {
        type: DataTypes.STRING,
        allowNull: true, // or false, depending on your requirements
      },
    },
    {
      sequelize,
      modelName: 'User_Biodata',
    },
  );
  // eslint-disable-next-line camelcase
  return User_Biodata;
};
