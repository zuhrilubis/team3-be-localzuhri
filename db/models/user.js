/* eslint-disable strict */

'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasOne(models.User_Biodata, { foreignKey: 'user_id' });
      User.hasMany(models.Game, { foreignKey: 'player_id', as: 'player' });
      User.hasMany(models.Game, { foreignKey: 'lawan_id' });
      User.hasMany(models.Room, { foreignKey: 'player1_id', as: 'player1' });
      User.hasMany(models.Room, { foreignKey: 'player2_id', as: 'player2' });
    }
  }
  User.init(
    {
      username: {
        type: DataTypes.STRING(100),
        unique: 'username_unique',
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        unique: 'email_unique',
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'User',
    },
  );
  return User;
};
