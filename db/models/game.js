/* eslint-disable strict */

'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Game.belongsTo(models.User, { foreignKey: 'player_id', as: 'player' });
      Game.belongsTo(models.User, { foreignKey: 'lawan_id' });
      Game.belongsTo(models.Room, { foreignKey: 'room_id' });
    }
  }
  Game.init(
    {
      player_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      pilihan: {
        type: DataTypes.STRING(100),
      },
      lawan_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      hasil: {
        type: DataTypes.STRING(100),
      },
      room_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'Rooms',
          key: 'id',
        },
      },
    },
    {
      sequelize,
      modelName: 'Game',
    },
  );
  return Game;
};
