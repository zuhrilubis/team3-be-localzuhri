/* eslint-disable strict */

'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Room.hasOne(models.Game, { foreignKey: 'room_id' });
      Room.belongsTo(models.User, { foreignKey: 'player1_id', as: 'player1' });
      Room.belongsTo(models.User, { foreignKey: 'player2_id', as: 'player2' });
    }
  }
  Room.init(
    {
      room_name: {
        type: DataTypes.STRING(100),
        allowNull: false,
        unique: 'room_name_unique',
      },
      player1_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      player2_id: {
        type: DataTypes.INTEGER,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      pilihan_p1: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      pilihan_p2: {
        type: DataTypes.STRING(100),
      },
      hasil_1: {
        type: DataTypes.STRING(100),
      },
      hasil_2: {
        type: DataTypes.STRING(100),
      },
      pemenang: {
        type: DataTypes.STRING(100),
      },
      status: {
        type: DataTypes.STRING(100),
      },
    },
    {
      sequelize,
      modelName: 'Room',
    },
  );
  return Room;
};
